import React, { useCallback, useEffect, useRef } from 'react';
import { StyleSheet, StatusBar, View, Text, FlatList } from 'react-native';
import Common from '../../utilities/Common';
import {GRAY_LIGHT, MAIN_COLOR} from './../../styles/colors';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';

const DetailScreen = ({navigation, route, selectedBill, addTransactionToBill}) => {

  const { data } = route.params;
  const [sum, setSum] = React.useState(0);

  useEffect(() => {
    console.warn(selectedBill);
    let sum = 0;
		for (let i = 0; i < selectedBill.transactions.length; i++) {
			if (selectedBill.transactions[i].isWithdrawal) {
				sum = sum + selectedBill.transactions[i].sum;
			} else {
				sum = sum - selectedBill.transactions[i].sum;
			}
		}
    setSum(sum);
  }, [selectedBill]);

  const renderRow = (item: object, index: number) => {
    return (<View style={{
      width: Common.getLengthByIPhone7(0),
      padding: Common.getLengthByIPhone7(10),
      borderBottomColor: GRAY_LIGHT,
      borderBottomWidth: 1,
    }}>
      <Text style={{
        // width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        color: MAIN_COLOR,
        fontSize: Common.getLengthByIPhone7(14),
        fontFamily: 'Montserrat-Regular',
        textAlign: 'left',
      }}
      allowFontScaling={false}>
        Дата: {item.date}
      </Text>
      <Text style={{
        // width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        color: MAIN_COLOR,
        fontSize: Common.getLengthByIPhone7(14),
        fontFamily: 'Montserrat-Regular',
        textAlign: 'left',
      }}
      allowFontScaling={false}>
        Тип: {item.isWithdrawal ? 'пополнение' : 'снятие'}
      </Text>
      <Text style={{
        // width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        color: MAIN_COLOR,
        fontSize: Common.getLengthByIPhone7(14),
        fontFamily: 'Montserrat-Regular',
        textAlign: 'left',
      }}
      allowFontScaling={false}>
        Сумма: {item.sum}
      </Text>
      <Text style={{
        // width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        color: MAIN_COLOR,
        fontSize: Common.getLengthByIPhone7(14),
        fontFamily: 'Montserrat-Regular',
        textAlign: 'left',
      }}
      allowFontScaling={false}>
        Комментарий: {item.comment}
      </Text>
    </View>);
  }

  return (
    <View style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>
      <Text style={{
        marginTop: Common.getLengthByIPhone7(16),
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        color: MAIN_COLOR,
        fontSize: Common.getLengthByIPhone7(16),
        fontFamily: 'Montserrat-Regular',
        textAlign: 'left',
      }}
      allowFontScaling={false}>
        Счет №{data !== null ? data.billId: ''}
      </Text>
      <Text style={{
        marginTop: Common.getLengthByIPhone7(10),
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        color: MAIN_COLOR,
        fontSize: Common.getLengthByIPhone7(18),
        fontFamily: 'Montserrat-Bold',
        textAlign: 'left',
      }}
      allowFontScaling={false}>
        {data !== null ? data.billName : ''}
      </Text>
      <Text style={{
        marginTop: Common.getLengthByIPhone7(10),
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        color: MAIN_COLOR,
        fontSize: Common.getLengthByIPhone7(18),
        fontFamily: 'Montserrat-Regular',
        textAlign: 'left',
      }}
      allowFontScaling={false}>
        Баланс: {sum}
      </Text>
      <Text style={{
        marginTop: Common.getLengthByIPhone7(10),
        color: MAIN_COLOR,
        fontSize: Common.getLengthByIPhone7(18),
        fontFamily: 'Montserrat-Bold',
        textAlign: 'left',
      }}
      allowFontScaling={false}>
        Транзакции
      </Text>
      <FlatList
        style={{
            flex: 1,
            backgroundColor: 'transparent',
            width: Common.getLengthByIPhone7(0),
            // marginBottom: 60,
            // marginTop: Common.getLengthByIPhone7(10),
        }}
        contentContainerStyle={{
            alignItems: 'center',
            justifyContent: 'flex-start',
        }}
        bounces={true}
        removeClippedSubviews={false}
        scrollEventThrottle={16}
        data={selectedBill.transactions}
        extraData={selectedBill.transactions}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => renderRow(item, index)}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({
	selectedBill: state.bills.selectedBill,
});

const mdtp = (dispatch: Dispatch) => ({
  addTransactionToBill: payload => dispatch.bills.addTransactionToBill(payload),
});

export default connect(mstp, mdtp)(DetailScreen);
