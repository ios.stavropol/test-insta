import React, { useEffect } from 'react';
import {TextInput, View, TouchableOpacity, Text, Alert} from 'react-native';
import Common from './../utilities/Common';
import {GRAY_LIGHT, MAIN_COLOR} from './../styles/colors';
import { setParams } from '../navigation/RootNavigation';

const NumericInput = ({onChangeText, min = 0, max, initialValue = 0, digits = 0, step = 1}) => {

	const [value, setValue] = React.useState(0);

	useEffect(() => {
		// setValue(initialValue.toString());
	}, [initialValue]);

	const normaliseValue = (value, decimals = 2) => {
		if (!value) {
		  return ''
		}
		if (value === '.') {
		  return value = '0.'
		}
	  
		var regex = new RegExp(`^-?\\d+(?:\\.\\d{0,${decimals}})?`)
		const decimalsNumber = value.toString().match(regex)[0]
		const parsed = parseFloat(decimalsNumber).toFixed(2)
		if (isNaN(parsed)) {
		  return '0'
		}
		return parsed
	}

	return (
		<View style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			height: Common.getLengthByIPhone7(40),
			marginTop: Common.getLengthByIPhone7(30),
		}}>
			<TextInput
				style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(40),
					paddingLeft: Common.getLengthByIPhone7(10),
					borderRadius: Common.getLengthByIPhone7(10),
					borderWidth: 1,
					borderColor: GRAY_LIGHT,
				}}
				placeholder={'Сумма'}
				keyboardType={'numeric'}
				allowFontScaling={false}
				numberOfLines={1}
				defaultValue={initialValue.toString()}
				underlineColorAndroid={'transparent'}
				onSubmitEditing={() => {
				
				}}
				onChangeText={text => {
					let parsedQty = text.replace(/[,]+/g, '.')
					if (Number.isNaN(parsedQty)) {
						parsedQty = 0;
					} else if (parsedQty > max) {
						parsedQty = max;
					} else if (parsedQty < min) {
						parsedQty = min;
					}
					
					setValue(parsedQty.toString());
					if (onChangeText) {
						onChangeText(parsedQty);
					}
				}}
				value={value}
			/>
			<View style={{
				position: 'absolute',
				top: 0,
				right: 0,
				bottom: 0,
			}}>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(20),
					alignItems: 'center',
					justifyContent: 'center',
					borderLeftColor: GRAY_LIGHT,
					borderLeftWidth: 1,
				}}
				onPress={() => {
					let newValue = parseFloat(value);
					newValue = newValue + step;
					newValue = Math.round(newValue * Math.pow(10, digits)) / Math.pow(10, digits);
					if (newValue < max) {
						setValue(newValue.toString());
						if (onChangeText) {
							onChangeText(newValue);
						}
					}
				}}>
					<Text style={{
						color: MAIN_COLOR,
						fontSize: Common.getLengthByIPhone7(18),
						fontFamily: 'Montserrat-Regular',
						textAlign: 'left',
						}}
					allowFontScaling={false}>
						+
					</Text>
				</TouchableOpacity>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(20),
					alignItems: 'center',
					justifyContent: 'center',
					borderLeftColor: GRAY_LIGHT,
					borderLeftWidth: 1,
					borderTopColor: GRAY_LIGHT,
					borderTopWidth: 1,
				}}
				onPress={() => {
					let newValue = parseFloat(value);
					newValue = newValue - step;
					newValue = Math.round(newValue * Math.pow(10, digits)) / Math.pow(10, digits);
					if (newValue > min) {
						setValue(newValue.toString());
						if (onChangeText) {
							onChangeText(newValue);
						}
					}
				}}>
					<Text style={{
						color: MAIN_COLOR,
						fontSize: Common.getLengthByIPhone7(18),
						fontFamily: 'Montserrat-Regular',
						textAlign: 'left',
						}}
					allowFontScaling={false}>
						-
					</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

export default NumericInput;