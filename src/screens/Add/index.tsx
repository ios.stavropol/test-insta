import React, { useCallback, useEffect, useRef } from 'react';
import { TouchableOpacity, TextInput, View, Text, Alert } from 'react-native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from '../../utilities/Common';
import {GRAY_LIGHT, MAIN_COLOR} from './../../styles/colors';
import { useNavigation } from '@react-navigation/native';

const AddScreen = ({bills, addBill}) => {

	const navigation = useNavigation();

	const [billId, setBillId] = React.useState('');
	const [billName, setBillName] = React.useState('');

	useEffect(() => {
		// setTimeout(() => {
		//     navigation.navigate('Login');
		// }, 3000);
	}, []);

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<TextInput
				style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(40),
					marginTop: Common.getLengthByIPhone7(30),
					paddingLeft: Common.getLengthByIPhone7(10),
					borderRadius: Common.getLengthByIPhone7(10),
					borderWidth: 1,
					borderColor: GRAY_LIGHT,
				}}
				allowFontScaling={false}
				placeholder={'№ счета'}
				numberOfLines={1}
				keyboardType={'number-pad'}
				underlineColorAndroid={'transparent'}
				onSubmitEditing={() => {
					
				}}
				ref={el => this.textInputRef = el}
				onChangeText={text => {
					setBillId(text);
				}}
				value={billId}
            />
			<TextInput
				style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(40),
					marginTop: Common.getLengthByIPhone7(30),
					paddingLeft: Common.getLengthByIPhone7(10),
					borderRadius: Common.getLengthByIPhone7(10),
					borderWidth: 1,
					borderColor: GRAY_LIGHT,
				}}
				allowFontScaling={false}
				placeholder={'Название счета'}
				numberOfLines={1}
				underlineColorAndroid={'transparent'}
				onSubmitEditing={() => {
					
				}}
				ref={el => this.textInputRef = el}
				onChangeText={text => {
					setBillName(text);
				}}
				value={billName}
            />
			<TouchableOpacity style={{
				marginTop: Common.getLengthByIPhone7(30),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(40),
				borderRadius: Common.getLengthByIPhone7(10),
				borderWidth: 2,
				borderColor: MAIN_COLOR,
				alignItems: 'center',
				justifyContent: 'center',
			}}
			onPress={() => {
				if (billId. length && billName.length) {
					addBill({
						billId,
						billName,
					});
					navigation.goBack(null);
				} else {
					Alert.alert('ERROR', 'Заполните все поля!');
				}
			}}>
				<Text style={{
					color: MAIN_COLOR,
					fontSize: Common.getLengthByIPhone7(18),
					fontFamily: 'Montserrat-Regular',
					textAlign: 'left',
				}}
				allowFontScaling={false}>
					Сохранить
				</Text>
			</TouchableOpacity>
		</View>
  );
};

const mstp = (state: RootState) => ({
	bills: state.bills.bills,
});

const mdtp = (dispatch: Dispatch) => ({
    addBill: payload => dispatch.bills.addBill(payload),
});

export default connect(mstp, mdtp)(AddScreen);
