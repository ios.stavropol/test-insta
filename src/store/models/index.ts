import { Models } from "@rematch/core";
import bills from "./bills";

export interface RootModel extends Models<RootModel> {
	bills: typeof bills;
}

export const models: RootModel = { bills };