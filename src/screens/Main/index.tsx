import React, { useCallback, useEffect, useRef } from 'react';
import { StyleSheet, StatusBar, View, TouchableOpacity, FlatList, Text } from 'react-native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { StackNavigationProp } from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';
import Common from '../../utilities/Common';
import {GRAY_LIGHT, MAIN_COLOR} from './../../styles/colors';
import { parseSync } from '@babel/core';

const MainScreen = ({bills, getBills, setSelectedBill, getBalance}) => {

	const navigation = useNavigation();

	useEffect(() => {
		getBills();
	}, []);

	const renderRow = (item: object, index: number) => {
		
		let sum = 0;
		for (let i = 0; i < item.transactions.length; i++) {
			if (item.transactions[i].isWithdrawal) {
				sum = sum + item.transactions[i].sum;
			} else {
				sum = sum - item.transactions[i].sum;
			}
		}

        return (<TouchableOpacity
            style={{
				width: Common.getLengthByIPhone7(0),
				// height: Common.getLengthByIPhone7(50),
				padding: Common.getLengthByIPhone7(10),
				borderBottomColor: GRAY_LIGHT,
				borderBottomWidth: 1,
				backgroundColor: 'white',
			}}
			onPress={() => {
				setSelectedBill(item);
				navigation.navigate('Detail', {data: item});
			}}
        >
			<Text style={{
				color: MAIN_COLOR,
				fontSize: Common.getLengthByIPhone7(16),
				fontFamily: 'Montserrat-Regular',
				textAlign: 'left',
			}}
			allowFontScaling={false}>
				№{item.billId}
			</Text>
			<Text style={{
				color: MAIN_COLOR,
				fontSize: Common.getLengthByIPhone7(18),
				fontFamily: 'Montserrat-Bold',
				textAlign: 'left',
			}}
			allowFontScaling={false}>
				{item.billName}
			</Text>
			<Text style={{
				color: MAIN_COLOR,
				fontSize: Common.getLengthByIPhone7(18),
				fontFamily: 'Montserrat-Regular',
				textAlign: 'left',
			}}
			allowFontScaling={false}>
				Баланс: {sum}
			</Text>
		</TouchableOpacity>);
    }

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'center',
        }}>
			<FlatList
                style={{
                    flex: 1,
                    backgroundColor: 'transparent',
                    width: Common.getLengthByIPhone7(0),
                    // marginBottom: 60,
                    // marginTop: Common.getLengthByIPhone7(10),
                }}
                contentContainerStyle={{
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                }}
                bounces={true}
                removeClippedSubviews={false}
                scrollEventThrottle={16}
                data={bills}
                extraData={bills}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => renderRow(item, index)}
            />
		</View>
	);
};

const mstp = (state: RootState) => ({
	bills: state.bills.bills,
});

const mdtp = (dispatch: Dispatch) => ({
    getBills: () => dispatch.bills.getBills(),
	setSelectedBill: payload => dispatch.bills.setSelectedBill(payload),
	getBalance: payload => dispatch.bills.getBalance(payload),
});

export default connect(mstp, mdtp)(MainScreen);
