//@ts-nocheck
import { createModel } from '@rematch/core';
import { StorageHelper } from './../../services';
import { Dispatch } from 'store';
import type { RootModel } from './../models';

type BillState = {
	bills: [],
	selectedBill: null,
};

const bills = createModel<RootModel>()({
	state: {
		bills: [],
		selectedBill: null,
	} as BillState, 
	reducers: {
		setBills: (state, payload: boolean) => ({
			...state,
			bills: payload,
		}),
		setSelectedBill: (state, payload: boolean) => ({
			...state,
			selectedBill: payload,
		}),
	},
	effects: (dispatch) => {
		return {
			async getBills(): Promise<any> {
				let array = [];
				const response = await StorageHelper.getData('bills');
				if (response) {
					let newResp = JSON.parse(response);
					if (newResp) {
						array = newResp;
					}
				}
				console.warn('getBills: ', array);
				dispatch.bills.setBills(array);
			},
			async addBill(newBill): Promise<any> {
				let array = [];
				const response = await StorageHelper.getData('bills');
				if (response) {
					let newResp = JSON.parse(response);
					if (newResp) {
						array = newResp;
					}
				}
				array.push({
					billId: newBill.billId,
					billName: newBill.billName,
					transactions: [],
				});
				dispatch.bills.setBills(array);
				StorageHelper.saveData('bills', JSON.stringify(array));
			},
			async addTransactionToBill(obj): Promise<any> {
				let array = [];
				console.warn('before obj: ', obj.bill);
				console.warn('transactionObj: ', obj.transaction);
				const response = await StorageHelper.getData('bills');
				if (response) {
					let newResp = JSON.parse(response);
					if (newResp) {
						array = newResp;
					}
				}
				for (let i = 0; i < array.length; i++) {
					if (array[i].billId == obj.bill.billId) {
						if (!array[i].transactions) {
							array[i].transactions = [];
						}
						let arr2 = array[i].transactions;
						arr2.push(obj.transaction);
						array[i].transactions = arr2;
						obj.bill = array[i];
						break;
					}
				}
				console.warn('array: ', array);
				console.warn('obj: ', obj);
				dispatch.bills.setBills(JSON.parse(JSON.stringify(array)));
				dispatch.bills.setSelectedBill(JSON.parse(JSON.stringify(obj.bill)));
				StorageHelper.saveData('bills', JSON.stringify(array));
			},
		}
	},
});

export default bills;