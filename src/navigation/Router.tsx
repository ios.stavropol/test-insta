import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { Image } from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {SafeAreaProvider} from 'react-native-safe-area-context';
import { colors } from '../styles';
import MainScreen from './../screens/Main';
import DetailScreen from './../screens/Detail';
import AddScreen from './../screens/Add';
import OperationScreen from './../screens/Operation';

import AddBill from '../components/buttons/AddBill';
import AddTransaction from '../components/buttons/AddTransaction';
import Close from '../components/buttons/Close';

export const Stack = createNativeStackNavigator();

export default function Router() {

    return (
        <NavigationContainer>
          <SafeAreaProvider>
            <Stack.Navigator>
              <Stack.Group>
                <Stack.Screen
                    name={'Main'}
                    component={MainScreen}
                    options={{
                        headerShown: true,
                        title: 'Счета',
                        headerRight: (props) => (
                          <AddBill
                            {...props}
                          />
                        ),
                    }}
                />
                <Stack.Screen
                    name={'Detail'}
                    component={DetailScreen}
                    options={{
                        headerShown: true,
                        title: 'Детали счета',
                        headerRight: (props) => (
                          <AddTransaction
                            {...props}
                          />
                        ),
                    }}
                />
              </Stack.Group>
              <Stack.Group
                screenOptions={{
                  presentation: 'modal',
                }}
              >
                  <Stack.Screen
                    name={'Add'}
                    component={AddScreen}
                    options={{
                      headerShown: true,
                      title: 'Создание счета',
                      headerRight: (props) => (
                        <Close
                          {...props}
                        />
                      ),
                    }}
                  />
                  <Stack.Screen
                    name={'Operation'}
                    component={OperationScreen}
                    options={{
                      headerShown: true,
                      title: 'Операция',
                      headerRight: (props) => (
                        <Close
                          {...props}
                        />
                      ),
                    }}
                  />
              </Stack.Group>
            </Stack.Navigator>
          </SafeAreaProvider>
        </NavigationContainer>
      );
}
