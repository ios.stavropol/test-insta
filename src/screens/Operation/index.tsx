import React, { useCallback, useEffect, useRef } from 'react';
import { StyleSheet, Switch, View, Text, TextInput, TouchableOpacity, Alert } from 'react-native';
import Common from '../../utilities/Common';
import {GRAY_LIGHT, MAIN_COLOR} from './../../styles/colors';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import NumericInput from './../../components/NumericInput';

const OperationScreen = ({navigation, route, selectedBill, addTransactionToBill}) => {

  const [isWithdrawal, setIsWithdrawal] = React.useState(false);
  const [sum, setSum] = React.useState(0);
  const [comment, setComment] = React.useState('');

  useEffect(() => {
    
  }, []);

  return (
    <View style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>
      <View style={{
        marginTop: Common.getLengthByIPhone7(30),
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
        <Text style={{
          color: MAIN_COLOR,
          fontSize: Common.getLengthByIPhone7(16),
          fontFamily: 'Montserrat-Regular',
          textAlign: 'left',
        }}
        allowFontScaling={false}>
          Снятие
        </Text>
        <Switch
          onValueChange={value => {
            console.warn(value);
            setIsWithdrawal(value);
          }}
          value={isWithdrawal}
        />
        <Text style={{
          color: MAIN_COLOR,
          fontSize: Common.getLengthByIPhone7(16),
          fontFamily: 'Montserrat-Regular',
          textAlign: 'left',
        }}
        allowFontScaling={false}>
          Пополнение
        </Text>
      </View>
      <NumericInput
        initialValue={0}
        min={0}
        max={100}
        digits={2}
        step={1}
        value={sum}
        onChangeText={text => {
          console.warn('onChangeText: ', text);
          setSum(text);
        }}
      />
      <TextInput
        style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          height: Common.getLengthByIPhone7(40),
          marginTop: Common.getLengthByIPhone7(30),
          paddingLeft: Common.getLengthByIPhone7(10),
          borderRadius: Common.getLengthByIPhone7(10),
          borderWidth: 1,
          borderColor: GRAY_LIGHT,
        }}
        allowFontScaling={false}
        placeholder={'Комментарий'}
        numberOfLines={1}
        underlineColorAndroid={'transparent'}
        onSubmitEditing={() => {
          
        }}
        onChangeText={text => {
          setComment(text);
        }}
        value={comment}
      />
      <TouchableOpacity style={{
        marginTop: Common.getLengthByIPhone7(30),
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        height: Common.getLengthByIPhone7(40),
        borderRadius: Common.getLengthByIPhone7(10),
        borderWidth: 2,
        borderColor: MAIN_COLOR,
        alignItems: 'center',
        justifyContent: 'center',
      }}
      onPress={() => {
        if (parseFloat(sum) > 0 && comment.length) {
          addTransactionToBill({bill: selectedBill,
            transaction: {
              date: new Date(),
              sum,
              comment,
              isWithdrawal,
            }
          });
          navigation.goBack(null);
        } else {
          Alert.alert('ERROR', 'Заполните все поля!');
        }
      }}>
        <Text style={{
          color: MAIN_COLOR,
          fontSize: Common.getLengthByIPhone7(18),
          fontFamily: 'Montserrat-Regular',
          textAlign: 'left',
        }}
        allowFontScaling={false}>
          Сохранить
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const mstp = (state: RootState) => ({
	selectedBill: state.bills.selectedBill,
});

const mdtp = (dispatch: Dispatch) => ({
  addTransactionToBill: payload => dispatch.bills.addTransactionToBill(payload),
});

export default connect(mstp, mdtp)(OperationScreen);
